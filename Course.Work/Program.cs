﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course.Work
{
    class Program
    {
        static void Main(string[] args)
        {
            int weight = 95;
            double[] pArr = { 0.9, 0.85, 0.88, 0.75, 0.9 };
            int[] cArr = { 4, 6, 12, 10, 5 };
            int[] wArr = { 4, 8, 7, 3, 5 };
            int count = weight - wArr.Sum();
            var mjArr = new int[pArr.Length];

            for (int n = 0; n < pArr.Length; n++)
            {
                var mj = new int[count];
                var fiJi = new double[count];
                double maxFiJi = 0;
                int maxMj = 0;
                if (n != 1)
                {
                    fiJi = n.GetFiJiAndMj(cArr, wArr, pArr, count, out mj);
                    maxFiJi = MaxFiJiAndMj(fiJi, mj, count, out maxMj);
                }
                else
                {
                    for (int j = 0; j < pArr.Length; j++)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            if (mj[i] <= wArr[j])
                                fiJi = j.GetFiJiAndMj(cArr, wArr, pArr, count, out mj);
                        }
                    }
                    maxFiJi = MaxFiJiAndMj(fiJi, mj, count, out maxMj);
                }
                mjArr[n] = maxMj;

                WriteArrays(fiJi, mj);
                Console.WriteLine($"max{n + 1}FiJi = {maxFiJi}");
                Console.WriteLine($"max{n + 1}mj = {maxMj}\n");
            }

            int wj = 0;
            int cj = 0;

            Console.Write("mj = ");
            WriteArray(mjArr);
            var res = TargetFunction.Resault(pArr, cArr, wArr, mjArr, out wj, out cj);
            Console.WriteLine($"\nW = {wj}\nC = {cj}\nResault = {res}");

            Console.ReadLine();
        }
        public static void WriteArrays(double[] arr1, int[] arr2)
        {
            for (int i = 0; i < arr1.Length; i++)
                Console.WriteLine($"{arr1[i]} {arr2[i]}");
        }
        public static void WriteArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
                Console.Write($"{arr[i]}  ");
        }
        public static double MaxFiJiAndMj(double[] fiJi, int[] mj, int count, out int maxMj)
        {
            double maxFiJi = 0;
            maxMj = 0;
            if (!fiJi.IsNullOrEmpty())
            {
                maxFiJi = fiJi.GetMax();
                for (int i = 0; i < count; i++)
                {
                    if (maxFiJi == fiJi[i])
                        maxMj = mj[i];
                }
            }
            return maxFiJi;
        }
    }
}