﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course.Work
{
    public static class TargetFunction
    {
        public static double[] GetFiJiAndMj(this int j, int[] c, int[] w, double[] p, int count, out int[] mj)
        {
            mj = new int[count + 1];
            var fj = new double[count + 1];
            for (int i = 0; i <= count; i++)
            {
                mj[i] = i / w[j];
                fj[i] = (1 - Math.Pow((1 - p[j]), mj[i] + 1)) * Math.Exp(-0.00108 * c[j] * mj[i]);
            }
            return fj;
        }

        public static double Resault(double[] p, int[] c , int[] w, int[] mj , out int wj , out int cj)
        {
            double res = 1;
            wj = 0;
            cj = 0;
            for (int i = 0; i < p.Length; i++)
            {
                res *= 1 - Math.Pow((1 - p[i]), mj[i] + 1);
                wj += w[i] * (mj[i] + 1);
                cj += c[i] * (mj[i] + 1);
            }
            return res;
        }
    }
}