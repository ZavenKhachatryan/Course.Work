﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Course.Work
{
    static class Helper
    {
        public static double GetMax(this double[] arr)
        {
            double max = arr[0];
            for (int i = 1; i < arr.Length; i++)
                if (arr[i] > max)
                    max = arr[i];
            return max;
        }
        public static int GetSum(this int[] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.Length; i++)
                sum += arr[i];
            return sum;
        }
        public static bool IsNullOrEmpty(this double[] arr)
        {
            return arr == null || arr.Length == 0;
        }
    }
}
